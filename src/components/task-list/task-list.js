import React from 'react';

class TaskList extends React.Component {

    completeDependencies(task) {
        if (!task.dependencyIds.length) {
            return true;
        }

        return task.dependencyIds.filter(dependantTaskId => {
            const taskState = this.props.state.tasks.find(eachTask => eachTask.id === dependantTaskId);
            return !taskState.completedAt;
        }).length === 0;
    }

    toggleTask = (task) => {
        task.completedAt = task.completedAt ? null : new Date();
        this.props.updateTasks(this.props.state.tasks);
        this.setState(this.props.state);
    }

    render() {
        const currentGroup = this.props.state.section;
        let tasks = this.props.state.tasks.filter(eachTask => eachTask.group === currentGroup);
        return tasks.map((task, taskIndex) => (
            <div className="task-row" key={taskIndex}>
                <div className="task-action">
                    {!this.completeDependencies(task) &&
                    <img src={require('../../assets/Locked.svg')} alt="Locked Task"/>
                    }
                    {this.completeDependencies(task) && task.completedAt &&
                    <img className="clickable" src={require('../../assets/Completed.svg')} alt="Completion Toggle"
                         onClick={() => this.toggleTask(task)}/>
                    }
                    {this.completeDependencies(task) && !task.completedAt &&
                    <img className="clickable" src={require('../../assets/Incomplete.svg')} alt="Completion Toggle"
                         onClick={() => this.toggleTask(task)}/>
                    }
                </div>
                <div className={"task-item " + (task.completedAt ? 'completed' : '')}>{task.task}</div>
            </div>
        ));
    }
}

export default TaskList;