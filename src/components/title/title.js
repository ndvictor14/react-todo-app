import React from 'react';

class Title extends React.Component {

    render() {
        const section = this.props.section;
        if (!section) {
            return <div className="list-header">
                <div className="title">Things To Do</div>
            </div>;
        } else {
            return <div className="list-header">
                <div className="title">{section}</div>
                <div className="text-link" onClick={this.props.showGroups}>All Groups</div>
            </div>;
        }
    }
}

export default Title;
