import React from 'react';
import './group-list.css';

class GroupList extends React.Component {

    expandSection = (section) => {
        this.props.updateSection(section);
    }

    CompletedTasks(groupTasks) {
        return <span>
            {groupTasks.filter(group => group.completedAt !== null).length}
            </span>
    }

    Section(groupSection) {
        return <div className="task-row" key={groupSection[0].group} onClick={() => this.expandSection(groupSection[0].group)}>
            <div className="task-action">
                <img src={require('../../assets/Group.svg')} alt="Group Toggle"/>
            </div>
            <div>
                <div className="task-item">{groupSection[0].group}</div>
                <div className="sub-text">{this.CompletedTasks(groupSection)} OF {groupSection.length} COMPLETE</div>
            </div>
        </div>;
    }

    render() {
        let sections = {};
        this.props.groups.forEach(groupInfo => {
            if (Object.keys(sections).indexOf(groupInfo.group) === -1) {
                sections[groupInfo.group] = [groupInfo];
            } else {
                sections[groupInfo.group].push(groupInfo);
            }
        });

        return Object.keys(sections).map(section => this.Section(sections[section]));
    }
}

export default GroupList;
