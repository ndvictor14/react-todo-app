import React from 'react';
import GroupList from './components/group-list/group-list';
import Title from './components/title/title';
import TaskList from './components/task-list/task-list';
import './App.css';

class App extends React.Component {

    constructor(props) {
        super(props);
        const tasks = [
            {
                id: 1,
                group: "Purchases",
                task: "Go to the bank",
                dependencyIds: [],
                completedAt: null,
            },
            {
                id: 2,
                group: "Purchases",
                task: "Buy hammer",
                dependencyIds: [1],
                completedAt: null,
            },
            {
                id: 3,
                group: "Purchases",
                task: "Buy wood",
                dependencyIds: [1],
                completedAt: null,
            },
            {
                id: 4,
                group: "Purchases",
                task: "Buy nails",
                dependencyIds: [1],
                completedAt: null,
            },
            {
                id: 5,
                group: "Purchases",
                task: "Buy paint",
                dependencyIds: [1],
                completedAt: null,
            },
            {
                id: 6,
                group: "Build Airplane",
                task: "Hammer nails into wood",
                dependencyIds: [2, 3, 4],
                completedAt: null,
            },
            {
                id: 7,
                group: "Build Airplane",
                task: "Paint wings",
                dependencyIds: [5, 6],
                completedAt: null,
            },
            {
                id: 8,
                group: "Build Airplane",
                task: "Have a snack",
                dependencyIds: [],
                completedAt: null,
            }
        ];

        this.state = {section: undefined, tasks: tasks};
    }

    clearSection= () => {
        this.setState({section: undefined});
    }

    setSection = (section) => {
        this.setState({section: section});
    }

    updateTasks = (tasks) => {
        this.setState({tasks: tasks});
    }

    render() {
        return (
            <div className="App">
                <Title section={this.state.section} showGroups={this.clearSection}></Title>
                {!this.state.section &&
                    <GroupList updateSection={this.setSection} groups={this.state.tasks}></GroupList>
                } {this.state.section &&
                    <TaskList state={this.state} updateTasks={this.updateTasks}></TaskList>
                }

            </div>

        );
    }
}

export default App;
